PWNED(8) - System Manager's Manual

# NAME

**pwned** - query the Have I Been Pwned: Pwned Password list

# SYNOPSIS

**pwned**

# DESCRIPTION

**pwned**
is a utility that checks if a password has been exposed in a previous data
breach by querying the Pwned Passwords list via the HaveIBeenPwned API.

# EXIT STATUS

The **pwned** utility exits&#160;0 on success, and&#160;&gt;0 if an error occurs.

# EXAMPLES

	$ pwned
	Password:
	Oh no - pwned!
	This password has been seen 37461 times before

# SEE ALSO

[https://haveibeenpwned.com/API/V3#PwnedPasswords](https://haveibeenpwned.com/API/V3#PwnedPasswords)

# AUTHORS

Sean Davies &lt;[sean@city17.org](mailto:sean@city17.org)&gt;

# CAVEATS

Depends on
curl(1).
